import Head from "next/head";

function BlogSSR({ posts }) {
  return (
    <>
      <Head>
        <title>Server-side Rendering</title>
      </Head>
      <ul>
        {posts.map((post) => (
          <li key={post.id}>{post.title}</li>
        ))}
      </ul>
    </>
  );
}

export async function getServerSideProps() {
  const res = await fetch(
    "https://codewithpeck-default-rtdb.asia-southeast1.firebasedatabase.app/posts.json"
  );
  const posts = await res.json();

  return {
    props: {
      posts,
    },
  };
}

export default BlogSSR;
