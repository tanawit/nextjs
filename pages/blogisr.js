import Head from "next/head";

function BlogISR({ posts }) {
  return (
    <>
      <Head>
        <title>Incremental Static Regeneration</title>
      </Head>
      <ul>
        {posts.map((post) => (
          <li key={post.id}>{post.title}</li>
        ))}
      </ul>
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch(
    "https://codewithpeck-default-rtdb.asia-southeast1.firebasedatabase.app/posts.json"
  );
  const posts = await res.json();

  return {
    props: {
      posts,
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 10, // In seconds
  };
}

export default BlogISR;
